# Avorion SLiM theme
Made in 2019 for the Public Domain

![Example](slim.jpg "Avorion SLiM theme")


Based on Zenwalk theme by [Hyperion](http://zenwalk.org/)


Uses [Xolonium font](https://fontlibrary.org/en/font/xolonium) (https://gitlab.com/sev/xolonium) when available

`/usr/share/fonts/Xolonium/Xolonium-{Bold,Regular}.otf`


You can preview a theme while Xorg is running with:

`$ slim -p /usr/share/slim/themes/<theme name>`


Useful resources:
- <https://github.com/PeteGozz/slim>
- <https://wiki.archlinux.org/index.php/SLiM>
- <https://wiki.gentoo.org/wiki/SLiM>


In /etc/slim.conf, you may want to review

`current_theme  Avorion`

`welcome_msg    %host login`

